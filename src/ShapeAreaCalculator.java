import java.util.Scanner;

public class ShapeAreaCalculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入形状名称（正方形、圆形 或 三角形）：");
        String shape = scanner.nextLine().trim().toLowerCase();

        if ("正方形".equals(shape)) {
            System.out.println("请输入正方形的边长：");
            double side = scanner.nextDouble();
            double area = side * side;
            System.out.println("正方形的面积是：" + area);
        }
        else if ("圆形".equals(shape)) {
            System.out.println("请输入圆形的半径：");
            double radius = scanner.nextDouble();
            double area = Math.PI * radius * radius;
            System.out.println("圆形的面积是：" + area);
        }
        else if ("三角形".equals(shape)) {
            System.out.println("请输入三角形的三条边长：");
            double a = scanner.nextDouble();
            double b = scanner.nextDouble();
            double c = scanner.nextDouble();
            double s = (a + b + c) / 2; // 半周长
            double area = Math.sqrt(s * (s - a) * (s - b) * (s - c)); // 海伦公式计算面积
            System.out.println("三角形的面积是：" + area);
        }
        else {
            System.out.println("不支持的形状类型。");
        }
        scanner.close();
    }
}